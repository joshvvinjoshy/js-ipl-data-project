const Papa = require('papaparse');
const fs = require('fs');

function convertCsvToJson(csvFilePath) {
  const csvData = fs.readFileSync(csvFilePath, 'utf8');
  const parsedData = Papa.parse(csvData, {
    header: true,
    skipEmptyLines: true,
  });
  // console.log(parsedData.meta);
  return parsedData.data;
}

const csvFilePath1='/home/joshvvinjoshy/mb/project_ipl/src/data/deliveries.csv'
const csvFilePath2='/home/joshvvinjoshy/mb/project_ipl/src/data/matches.csv'
const deliveries = convertCsvToJson(csvFilePath1);
const matches = convertCsvToJson(csvFilePath2);
const allmatches = JSON.stringify(matches, null, 2);
const alldeliveries = JSON.stringify(deliveries, null, 2);
fs.writeFileSync('/home/joshvvinjoshy/mb/project_ipl/src/data/alldeliveries.json', alldeliveries);
fs.writeFileSync('/home/joshvvinjoshy/mb/project_ipl/src/data/allmatches.json', allmatches);

// console.log(deliveries);
// console.log(matches);
// module.exports = {matches, deliveries};
