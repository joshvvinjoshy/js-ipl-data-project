const matches = require('../data/allmatches.json');
const deliveries = require('../data/alldeliveries.json');
const fs = require('fs');

function wontosswonmatch(){   
    // console.log(parsed_data.all_matches_list);
    // const matches = parsed_data.matches;
    let won = {};
    let count = 0;
    matches.map((match) => {
        const toss_winner = match.toss_winner;
        const winner = match.winner;
        if(toss_winner == winner){
            count++;
            if(won[winner] == undefined){
                won[winner] = 1;
            }
            else{
                won[winner] ++;
            }
        }
    });
    // console.log(won);
    // console.log(count);
    const res = JSON.stringify(won, null, 2) + '\n';
    fs.writeFileSync("../public/output/wonTossWonMatch.json", res, 'utf-8');
    // console.log(res);

}
wontosswonmatch();
// module.exports = match_dict;