const matches = require('../data/allmatches.json');
const deliveries = require('../data/alldeliveries.json');
const match_dict = require('./1-matches-per-year.cjs');
const fs = require('fs');
// console.log(match_dict);
function strikerate(){
    let strikerateyearwise = {};
    let ballsfaced = {};
    let runs = {};
    let season_ids = {};
    // let matches = parsed_data.matches;
    // let deliveries = parsed_data.deliveries;
    matches.map((match) =>{
        const year = match.season;
        const id = match.id;
        if(season_ids[year] == undefined){
            season_ids[year] = [id];
        }
        else{
            season_ids[year].push(id);
        }
    });
    // console.log(season_ids);
    // console.log(runs);
    // const deliveries = parsed_data.deliveries;
    
    deliveries.map((del) =>{
        let y = '';
        const season_ids_list = Object.entries(season_ids);
        const match_id = del.match_id;
        const batsman = del.batsman;
        const batsman_runs = del.batsman_runs;
        const noball_runs = del.noball_runs;
        const wide_runs = del.wide_runs;
        season_ids_list.map(([year, match_ids]) =>{
            if(match_ids.includes(match_id)){
                y=year;
                return true;                
            }
        });
        if(runs[batsman] == undefined){
            runs[batsman] = {};
        }
        if(ballsfaced[batsman] == undefined){
            ballsfaced[batsman] = {};
        }
        if(runs[batsman][y] == undefined){
            runs[batsman][y] = parseInt(batsman_runs);
        }
        else{
            runs[batsman][y] += parseInt(batsman_runs);
        }
        if(ballsfaced[batsman][y] == undefined){
            // wide_runs,noball_runs
            if(wide_runs == 0 && noball_runs == 0)
                ballsfaced[batsman][y] = 1;
            else    
                ballsfaced[batsman][y] = 0;
        }
        else{
            if(wide_runs == 0 && noball_runs == 0)
                ballsfaced[batsman][y] ++;
        }
    });
    // console.log(runs);
    // console.log(ballsfaced);
    const runs_list = Object.entries(runs);
    runs_list.map(([batsman, years]) =>{
        // console.log(runs[run]);
        // else{
        const years_list = Object.entries(years);
        years_list.map(([year, runs_of_batsman]) =>{
            if(strikerateyearwise[batsman] == undefined){
                strikerateyearwise[batsman] = {};
            }
            if(strikerateyearwise[batsman][year] == undefined){
                strikerateyearwise[batsman][year] = 0;
            }
            strikerateyearwise[batsman][year] = (runs_of_batsman/ballsfaced[batsman][year]) * 100;
        });
        // }
    });
    // console.log(strikerateyearwise);
    const res = JSON.stringify(strikerateyearwise, null, 2) + '\n';
    fs.writeFileSync("../public/output/strikeRate.json", res, 'utf-8');
    // console.log(res);
}
strikerate();