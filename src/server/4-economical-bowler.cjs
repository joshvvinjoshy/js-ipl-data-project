const matches = require('../data/allmatches.json');
const deliveries = require('../data/alldeliveries.json');
const fs = require('fs');
function economical_bowlers(){
    const top = {};
    let match_ids = [];
    matches.map((match) =>{
        const match_id = match.id;
        if(match.season == '2015'){
            match_ids.push(match_id);
        }
    });
    let overs = {};
    let runs = {};
    deliveries.map((del) =>{
        const match_id = del.match_id;
        const bowler = del.bowler;
        const total_runs = del.total_runs;
        const over = del.over;
        if(match_ids.includes(match_id)){
            if(runs[bowler] == undefined){
                runs[bowler] = parseInt(total_runs);
            }
            else{
                runs[bowler] += parseInt(total_runs);
            }
            if(overs[bowler] == undefined){
                overs[bowler] = {};
                overs[bowler][match_id] = [del.over];
            }
            else{
                if(overs[bowler][match_id] == undefined){
                    overs[bowler][match_id] = [over];
                }
                else{
                    if(!overs[bowler][match_id].includes(over)){
                        overs[bowler][match_id].push(over);
                    }
                }
            }
        }
    });
    const result ={};
    const runs_list = Object.entries(runs);
    runs_list.map(([bowler, run]) => {
        let total_overs = 0;
        const match_ids1 = overs[bowler];
        // console.log(match_ids1);
        const match_ids1_list = Object.entries(match_ids1);
        match_ids1_list.map(([match_id1, overs_for_each_match_id]) => {
            total_overs += overs_for_each_match_id.length;
        });
        result[bowler]= run/total_overs;
    });
    // console.log(result);
    const result_list = Object.entries(result);
    result_list.sort((a, b) => a[1] - b[1]); 
    const sortedResult = Object.fromEntries(result_list);
    // // console.log(sortedObject);
    const top_10_bowlers_with_lowest_economy_list = Object.entries(sortedResult).slice(0, 10);
    const top_10_bowlers_with_lowest_economy_object = Object.fromEntries(top_10_bowlers_with_lowest_economy_list);
    // console.log(top_10_bowlers_with_lowest_economy_object);
    const answer = JSON.stringify(top_10_bowlers_with_lowest_economy_object, null, 2) + '\n';
    fs.writeFileSync("../public/output/economicalBowler.json", answer, 'utf-8');
    
}
economical_bowlers();