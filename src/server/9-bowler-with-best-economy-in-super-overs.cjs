const matches = require('../data/allmatches.json');
const deliveries = require('../data/alldeliveries.json');
// const match_dict = require('./1-matches-per-year.cjs');
const fs = require('fs');
// console.log(match_dict);
function superovereconomy(){
    let runs = {};
    let superovers = {};
    deliveries.map((del) =>{
        const bowler = del.bowler;
        const is_super_over = del.is_super_over;
        const match_id = del.match_id;
        const over = del.over;
        const total_runs = del.total_runs;
        if(is_super_over == '1'){
            // console.log(deliveries[del]);
            if(superovers[bowler] == undefined){
                superovers[bowler] = {};
            }
            if(superovers[bowler][match_id] == undefined){
                superovers[bowler][match_id] = [over];
            }
            else{
                if(!superovers[bowler][match_id].includes(over))
                superovers[bowler][match_id].push(over);
            }
            if(runs[bowler] == undefined){
                runs[bowler] = parseInt(total_runs);
            }
            else{
                runs[bowler] += parseInt(total_runs);
            }
        }
    });

    let answer = {};
    let m = 100;
    const superovers_list = Object.entries(superovers);
    superovers_list.map(([bowler, match_ids]) =>{
        let l = 0;
        const match_ids_list = Object.entries(match_ids);
        match_ids_list.map(([match_id,overs_list]) =>{
            l += overs_list.length;
        });
        answer[bowler] = runs[bowler]/l;
        if(answer[bowler] < m){
            m = answer[bowler];
        }        
    });
    // console.log(answer);
    let rez = {};
    const answer_list = Object.entries(answer);
    answer_list.map(([player, eco])=>{
        if(eco == m){
            rez[player] = eco;
        }
    });
    // console.log(rez);
    const res = JSON.stringify(rez, null, 2) + '\n';
    fs.writeFileSync("../public/output/bowlerWithBestEconomyInSuperOvers.json", res, 'utf-8');
    // console.log(res);
}
superovereconomy();