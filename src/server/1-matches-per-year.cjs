const matches = require('../data/allmatches.json');
const fs = require('fs');
let match_dict = {};
function matchesperyear(){   
    matches.map((match) =>{
        const year = match.season;
        if(match_dict[year] == undefined){
            match_dict[year] = 1;
        }
        else{
            match_dict[year]++;
        }
    });
    // console.log(match_dict);
    const res = JSON.stringify(match_dict, null, 2) + '\n';
    fs.writeFileSync("../public/output/matchesPerYear.json", res, 'utf-8');
    // console.log(res);

}
matchesperyear();
module.exports = match_dict;