const matches = require('../data/allmatches.json');
const deliveries = require('../data/alldeliveries.json');

const fs = require('fs');
// console.log(match_dict);
function extraruns(){
    const extra = {};
    let match_ids = [];
    matches.map((match) =>{
        const year = match.season;
        const match_id = match.id;
        if(year == '2016'){
            match_ids.push(match_id);
        }
    });
    // console.log(match_ids);
    deliveries.map((delivery) =>{
        const match_id = delivery.match_id;
        const bowling_team = delivery.bowling_team;
        const extra_runs = delivery.extra_runs;
        if(match_ids.includes(match_id)){
            if(extra[bowling_team] == undefined){
                extra[bowling_team] = parseInt(extra_runs);
            }
            else{
                extra[bowling_team] += parseInt(extra_runs);
            }
        }
    });
    // console.log(extra);
    const res = JSON.stringify(extra, null, 2) + '\n';
    fs.writeFileSync("../public/output/extraRuns.json", res, 'utf-8');    
}
extraruns();