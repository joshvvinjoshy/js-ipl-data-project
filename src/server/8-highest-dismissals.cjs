const matches = require('../data/allmatches.json');
const deliveries = require('../data/alldeliveries.json');
// const match_dict = require('./1-matches-per-year.cjs');
const fs = require('fs');
// console.log(match_dict);
function highestdismissals(){
    let dismissals = {};
    
    deliveries.map((del) =>{
        const player_dismissed = del.player_dismissed;
        const bowler = del.bowler;
        const fielder = del.fielder;
        const dismissal_kind = del.dismissal_kind;
        if(player_dismissed.length > 0){
            // console.log(deliveries[del]);
            if(dismissal_kind != 'run out'){
                if(dismissals[player_dismissed] == undefined){
                    dismissals[player_dismissed] = {};
                    dismissals[player_dismissed][bowler] = 1;
                }
                else{
                    if(dismissals[player_dismissed][bowler] == undefined){
                        dismissals[player_dismissed][bowler] = 1;
                    }
                    else{
                        dismissals[player_dismissed][bowler] ++;
                    }
                }
            }
            else{
                // console.log(deliveries[del]);
                if(dismissals[player_dismissed] == undefined){
                    dismissals[player_dismissed] = {};
                    dismissals[player_dismissed][fielder] = 1;
                }
                else{
                    if(dismissals[player_dismissed][fielder] == undefined){
                        dismissals[player_dismissed][fielder] = 1;
                    }
                    else{
                        dismissals[player_dismissed][fielder] ++;
                    }
                }
            }
        }
    });
    // console.log(dismissals);

    let answer = {};
    const dismissals_list = Object.entries(dismissals);
    dismissals_list.map(([player_dismissed, dismissed_bys]) =>{
        let max = 0;
        const dismissed_bys_list = Object.entries(dismissed_bys)
        dismissed_bys_list.map(([dismissed_by,no_of_times_dismissed]) =>{
            max = Math.max(max, no_of_times_dismissed);
        });
        answer[player_dismissed] = max;
    });
    // console.log(answer);
    const res = JSON.stringify(answer, null, 2) + '\n';
    fs.writeFileSync("../public/output/highestDismissals.json", res, 'utf-8');
    // console.log(res);
}
highestdismissals();